import { createSlice } from '@reduxjs/toolkit'

export const TodoSlice = createSlice({
    name: 'todo',
    initialState: {
        todoList: []
    },
    reducers: {
        addTodo: (state, action) => {
            state.todoList = [...state.todoList, action.payload]
        },
        updateTodo: (state, action) => {
            state.todoList.forEach(item => {
                if (item.id === action.payload.id) {
                    item.done = !item.done;
                }
            })
        },
        deleteTodoItem: (state, action) => {
            state.todoList = state.todoList.filter(item => 
                item.id !== action.payload.id
            )
        }
    }
})

export const { addTodo, updateTodo, deleteTodoItem } = TodoSlice.actions
export default TodoSlice.reducer