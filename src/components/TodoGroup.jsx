import { TodoItem } from "./ToDoItem" 
import "./TodoGroup.css"
export const TodoGroup = (props) => {
    return (
        <div className="todo-group">
            {props.todoList.map((value, index) =>
                (<TodoItem key={index} value={value}></TodoItem>))}
        </div>
    )
}
