import "./TodoItem.css"
import { useDispatch } from "react-redux";
import { updateTodo, deleteTodoItem } from "./TodoSlice";
import deleteIcon from "../icon/delete.png"

export const TodoItem = (props) => {
    const dispatch = useDispatch();

    const handleClick = () => {
        dispatch(updateTodo(props.value));
    }

    const deleteHandle = () => {
        dispatch(deleteTodoItem(props.value));
    }

    return (
        <div className='todo-item'>
            <p className={props.value.done ? 'todo-item-text-finish' : 'todo-item-text'} onClick={handleClick}>{props.value.text}</p>
            <img src={deleteIcon} alt='delete-todo' className="itemIcon" onClick={deleteHandle}/>
        </div>
    )
}