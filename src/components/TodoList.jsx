import { TodoGroup } from "./TodoGroup";
import { TodoItemGenerator } from "./ToDoItemGenerator";
import "./TodoList.css"
import { addTodo } from "./TodoSlice";
import { useDispatch } from "react-redux";
import { useSelector } from "react-redux";

export const TodoList = () => {
    const todoList = useSelector(state => state.todo.todoList)
    const dispatch = useDispatch();

    const handleUpdateTodoList = (newTodo) => {
        dispatch(addTodo(newTodo))
    }
    return (
        <div className="todo-list">
            <p>Todo List</p>
            <TodoGroup todoList={todoList}></TodoGroup>
            <TodoItemGenerator onChange={handleUpdateTodoList}></TodoItemGenerator>
        </div>
    )
}
