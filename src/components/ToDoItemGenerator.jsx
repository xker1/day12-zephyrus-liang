import { useState } from "react";
import "./TodoGenerator.css"

export const TodoItemGenerator = (props) => {
    const [content, setContent] = useState('');

    const handleChangeInput = (e) => {
        setContent(e.target.value);
    }

    const handleChange = () => {
        if (content.trim() !== '') {
            props.onChange({
            id: new Date().toString(),
            text: content,
            done: false,
        })
        setContent('');
        }
    }

    const handleKepUp = (event) => {
        if (event.keyCode === 13) {
            handleChange()
        }
    }
    
    return (
        <div>
            <input type="text" value={content} onChange={handleChangeInput}
                onKeyUp={handleKepUp} placeholder="input a new todo here"></input>
            <button className="button" onClick={handleChange}>add</button>
        </div>
    )
}
