O:
 1. Conduct code review in the morning and review yesterday's knowledge.
 2. Learn the knowledge of Redux's line pipe, and use Redux to optimize the problems encountered by React in code review.
 3. On the basis of yesterday's homework, do a lot of exercises on the learned Redux knowledge.
 4. Learn how to test React and write a few examples.
R:
 Difficult and a little hard to get started
I: 
 1. Today I learned the use of Redux, which is based on the react I learned yesterday, and the difficulty is relatively reduced.
 2. Today's exercise helped me better understand the use of redux and react, but the unfamiliarity with JS syntax still bothers me.
D:
 I am re-learning the related writing of JS and using it in after-class exercises, so that it will be easier to learn.